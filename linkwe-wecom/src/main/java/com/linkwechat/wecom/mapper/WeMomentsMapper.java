package com.linkwechat.wecom.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.linkwechat.wecom.domain.WeMoments;

public interface WeMomentsMapper extends BaseMapper<WeMoments> {
}
